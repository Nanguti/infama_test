<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FlighMoviesController; 



// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'FlightMoviesController@index');
Route::post('flight-movies', 'FlightMoviesController@flightLength')->name('flight-movies');
