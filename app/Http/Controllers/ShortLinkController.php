<?php
  
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\Models\ShortLink;
use Illuminate\Support\Str;
use Illuminate\Routing\UrlGenerator;

  
class ShortLinkController extends Controller
{

    protected $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }

    public function index()
    {
        
        $shortLinks = ShortLink::latest()->get();
        $url  = $this->url->to('/');

        $all_urls = [];
        foreach ($shortLinks as $key => $shortLink) {

            $all_urls []=$key.' Shorter url ->'.$url.'/'.$shortLink->code.' Original url->'.$shortLink->link;
        }
        return $all_urls;
   
    }
     
    public function store(Request $request)
    {
        $request->validate([
           'link' => 'required|url'
        ]);

   
        $input['link'] = $request->link;
        $input['code'] = Str::random(6);
   
        $model = ShortLink::create($input);

        $url  = $this->url->to('/');
        $shorter_url  = $url.'/'.$model->code;

        return 'Shorter Url: '.$shorter_url;
    }
   
    public function shortenLink($code)
    {
        $find = ShortLink::where('code', $code)->first();
   
        return redirect($find->link);
    }
}