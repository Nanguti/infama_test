<?php
  
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\Models\Movies;
use App\Models\Flights;
use App\Models\Playlist;
use Illuminate\Support\Str;
use Illuminate\Routing\UrlGenerator;

  
class FlightMoviesController extends Controller
{

    protected $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url;
    }

    public function index()
    {
        $flights = Flights::latest()->get();
        $movies = Movies::latest()->orderBy('id', 'asc')->get();
        return view('flight-movies',['movies'=>$movies, 'flights'=> $flights]);
        
   
    }
     
    public function flightLength(Request $request)
    {
        $request->validate([
           'movieDuration' => 'required',
           'flight_id' => 'required',
        ]);

        
        $count_movies = count($request->movieDuration);
        $flight = Flights::where('id', $request->flight_id)->first();
        if ($count_movies == 2){

            $lengthofFlight = $flight->duration;
            $flight_minutes = explode(":", $lengthofFlight);
            $flight_minutes  = ($flight_minutes[0]*60) + ($flight_minutes[1]);

            $movieDuration = $request->movieDuration;
            //firts movie and second movie in minutes
            $first_movie = $movieDuration[0];
            $fist_movie = explode(":", $first_movie);
            $fist_movie  = ($fist_movie[0]*60) + ($fist_movie[1]);

            $second_movie = $movieDuration[1];
            $second_movie = explode(":", $second_movie);
            $second_movie  = ($second_movie[0]*60) + ($second_movie[1]);

            $total_duration = $fist_movie + $second_movie;



            if ($flight_minutes == $total_duration) {

                $playlist = new Playlist;
                $playlist->flight_id = $request->flight_id;
                $playlist->playlist = json_encode($request->movieDuration);
                $playlist->save();
                return back()->with('success', 'Movie playlist created successfully for a whole flight time');
            }
            else
            {
               return back()->with('error', 'Could not create a movie playlist. Make sure flight duration is equal to movie playlist duration.');
            }  
        }
        else {
            return back()->with('error', 'You must select two movies.');
        }
   
    }
   
}