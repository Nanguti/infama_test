<!DOCTYPE html>
<html>
<head>
    <title>Flight & Movies</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="public/css/app.css">
</head>
<body>
   
    <div class="container">
       <h3 class="flight_title">Flight & Movies</h3>
        <div class="card">
            
            <div class="card-header">
                
                <div class="flight_title">
                    
                    <form method="POST" action="{{ route('flight-movies') }}">
                        @csrf

                        <div class="card-body">

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif

                            @if ($message = Session::get('error'))
                                <div class="alert-error alert-danger.">
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif


                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="col-xs-6" style="margin: 20px 0px">
                                <h3 class="">Select Movie:</h3>
                                <select class="form-control" name="flight_id">
                                <!-- <option>Select movie</option> -->
                                @foreach ($flights as $key => $value)
                                    <option value="{{ $value->id }}" > 
                                        {{ $value->start.' to '.$value->destination.', Flight Time '.$value->duration }} 
                                    </option>
                                @endforeach    
                                </select>
                            </div>


                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Movie Name</th>
                                        <th>Movie Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($movies as $row)
                                        <tr>
                                            <td>{{ $row->id }}</td>
                                            <td>
                                              <span class="custom-checkbox">
                                                <input type="checkbox" name="movieDuration[]" value="{{ $row->duration}}">
                                                <label for="{{ $row->id }}"></label>
                                              </span>{{$row->name}}
                                           </td>
                                           <td>{{ $row->duration }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                            
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div>   
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- script to prevent selection of more than two movies -->
    <script type="text/javascript">
        var $checkboxes = $('input[type=checkbox]');

        $checkboxes.change(function () {
            if (this.checked) {
                if ($checkboxes.filter(':checked').length == 2  ) {
                    $checkboxes.not(':checked').prop('disabled', true);
                }
            } else {
                $checkboxes.prop('disabled', false);
            }
        });

    </script>
</body>
</html>