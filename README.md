Laravel (PHP framework). 
Festures of the above project: 

1: API for a URL shortener.

routes:
 /api/generate-shorten-link this a post route used to generate short urls.
/api/generate-shorten-link this a get route ussed to display all routes.

check the code in app\Http\Controllers\ShortLinkController

demo-link tested with postman:
https://demotest.brickwallpr.co.ke/api/generate-shorten-link

2: Feature that allows the selection of two movies whose total duration is equal to flight time. 
The feature demonistrates (MVC)- Model View and Controller. 

Feature uses app\Http\Controllers\FlightMoviesController controller to
accomplish with resources\views\flight-movies.blade as the view file. 

Find the demo-link 
https://demotest.brickwallpr.co.ke/

