-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2021 at 09:28 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `infama`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flights`
--

CREATE TABLE `flights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` time NOT NULL,
  `start` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `flights`
--

INSERT INTO `flights` (`id`, `name`, `duration`, `start`, `destination`, `created_at`, `updated_at`) VALUES
(1, 'KQ-145', '03:45:12', 'Moyale', 'Nairobi', NULL, NULL),
(2, 'KQ-145', '04:30:12', 'Khartum', 'Kampala', NULL, NULL),
(3, 'KQ-145', '07:45:12', 'Nairob1', 'Dubai', NULL, NULL),
(5, 'KQ-145', '02:45:12', 'Nakuru', 'Nairobi', NULL, NULL),
(6, 'KQ-145', '05:30:12', 'Gambia', 'Burkina', NULL, NULL),
(7, 'KQ-145', '04:51:12', 'Harare', 'Entebe', NULL, NULL),
(8, 'KQ-145', '07:45:12', 'Dubai', 'Nairobi', NULL, NULL),
(9, 'KQ-145', '03:50:12', 'Nairobi', 'Kisumu', NULL, NULL),
(10, 'KQ-145', '04:02:12', 'Kigali', 'Edloret', NULL, NULL),
(11, 'KQ-145', '07:30:12', 'Entebe', 'Kisumu', NULL, NULL),
(12, 'KQ-145', '02:45:12', 'Naiorbi', 'Eldoret', NULL, NULL),
(13, 'KQ-145', '03:20:12', 'Mombasa', 'Kisumu', NULL, NULL),
(14, 'KQ-145', '05:05:12', 'Dubai', 'Bahrain', NULL, NULL),
(15, 'KQ-145', '06:05:12', 'Lagos', 'Dodoma', NULL, NULL),
(16, 'KQ-145', '08:00:12', 'Naiorobi', 'Tripoli', NULL, NULL),
(17, 'KQ-145', '07:20:12', 'Abuja', 'Bujumbura', NULL, NULL),
(18, 'KQ-145', '09:02:12', 'Armstardam', 'Nairobi', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Et dolor.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(2, 'Adipisci.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(3, 'Aliquam.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(4, 'Assumenda.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(5, 'Ea et ab.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(6, 'Qui.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(7, 'Veniam.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(8, 'Veritatis.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(9, 'Voluptas.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(10, 'Expedita.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(11, 'Ipsum.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(12, 'Ex.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(13, 'Officia.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(14, 'Et.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(15, 'Sint.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(16, 'Qui.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(17, 'Quis.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(18, 'Odio sed.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(19, 'Ab esse.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(20, 'Inventore.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(21, 'Aliquid.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(22, 'Dolorem.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(23, 'Soluta.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(24, 'Eos quo.', '2021-01-20 01:04:27', '2021-01-20 01:04:27'),
(25, 'Aliquid.', '2021-01-20 01:04:27', '2021-01-20 01:04:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_17_103049_create_mpesa_transactions_table', 1),
(5, '2021_01_17_122905_create_packages_table', 2),
(6, '2021_01_17_152424_create_posts_table', 3),
(7, '2021_01_18_043357_create_books_table', 4),
(8, '2021_01_18_072954_create_ratings_table', 4),
(9, '2021_01_20_033642_create_items_table', 5),
(10, '2021_01_20_041333_create_shops_table', 6),
(11, '2021_01_25_040419_create_state_city_tables', 7),
(12, '2021_01_25_052225_create_categories_table', 8),
(13, '2021_03_28_131539_create_short_links_table', 9),
(14, '2021_03_28_174945_create_flights_table', 10),
(15, '2021_03_28_175126_create_movies_table', 11),
(16, '2021_03_29_070953_create_playlists_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `name`, `duration`, `created_at`, `updated_at`) VALUES
(1, 'hunters', '04:00:00', NULL, NULL),
(2, 'Amigos', '02:45:12', NULL, NULL),
(3, 'pilots', '01:45:12', NULL, NULL),
(4, 'Breaking Bad', '00:45:12', NULL, NULL),
(5, 'Narcos', '02:02:12', NULL, NULL),
(6, 'El Chapo', '02:00:12', NULL, NULL),
(7, 'Le cass', '01:50:12', NULL, NULL),
(8, 'Movies star', '03:45:12', NULL, NULL),
(9, 'Guzman', '01:20:12', NULL, NULL),
(10, 'American Ninja', '03:45:12', NULL, NULL),
(11, 'Nairobi Half', '02:05:12', NULL, NULL),
(12, 'Crime and Justice', '04:00:12', NULL, NULL),
(13, 'Black Panther', '02:05:12', NULL, NULL),
(14, 'Power', '01:30:12', NULL, NULL),
(15, 'Power book 2', '02:12:12', NULL, NULL),
(16, 'Maaster', '02:03:12', NULL, NULL),
(17, 'Lords of the siky', '02:48:12', NULL, NULL),
(18, 'Another one', '01:55:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE `playlists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `flight_id` int(11) NOT NULL,
  `playlist` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `playlists`
--

INSERT INTO `playlists` (`id`, `flight_id`, `playlist`, `created_at`, `updated_at`) VALUES
(1, 1, '[\"02:00:12\",\"01:45:12\"]', '2021-03-29 04:17:46', '2021-03-29 04:17:46');

-- --------------------------------------------------------

--
-- Table structure for table `short_links`
--

CREATE TABLE `short_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `short_links`
--

INSERT INTO `short_links` (`id`, `code`, `link`, `created_at`, `updated_at`) VALUES
(1, 'G4sNg0', 'https://www.seklologistics.com/', '2021-03-28 10:32:04', '2021-03-28 10:32:04'),
(2, 'vWy8XG', 'http://localhost/commerce/jiji/mpesa_api/public/generate-shorten-link', '2021-03-28 10:32:28', '2021-03-28 10:32:28'),
(3, 'rGya9H', 'http://localhost/commerce/jiji/mpesa_api/public/generate-shorten-link', '2021-03-28 10:37:11', '2021-03-28 10:37:11'),
(4, '5Dq8iK', 'https://mbuthiaelias16.wixsite.com/consiliumgroup', '2021-03-28 11:10:19', '2021-03-28 11:10:19'),
(5, 'XDRvqT', 'https://mbuthiaelias16.wixsite.com/', '2021-03-28 11:11:07', '2021-03-28 11:11:07'),
(6, 'xuUYkT', 'https://mbuthiaelias16.wixsite.com/', '2021-03-28 11:13:04', '2021-03-28 11:13:04'),
(7, 'NB8QyM', 'https://mbuthiaelias16.wixsite.com/', '2021-03-28 11:13:30', '2021-03-28 11:13:30'),
(8, 'V8eij1', 'https://ptere.wixsite.com/', '2021-03-28 11:17:03', '2021-03-28 11:17:03'),
(9, 'fQI82b', 'https://www.facebook.com/Snr.Nanguti/', '2021-03-28 11:18:34', '2021-03-28 11:18:34'),
(10, 'sMaefB', 'https://www.facebook.com/Snr.Nanguti/', '2021-03-28 11:20:34', '2021-03-28 11:20:34'),
(11, 'pS5KWl', 'https://www.mwalimuplus.com/', '2021-03-28 11:50:31', '2021-03-28 11:50:31'),
(12, 'tORQNU', 'https://www.mwalimuplus.com/', '2021-03-28 11:51:33', '2021-03-28 11:51:33'),
(13, 'm6ncCg', 'https://www.mwalimuplus.com/', '2021-03-28 11:52:01', '2021-03-28 11:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nanguti Wanyonyi', 'g.nanguti@gmail.com', NULL, '$2y$10$VcmYQa6z1GYIMEhmzEl4ieG26Rp7RDH.6SWDgih.P5.Fyz0aUwyrm', NULL, '2021-01-18 05:54:32', '2021-01-18 05:54:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `flights`
--
ALTER TABLE `flights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `short_links`
--
ALTER TABLE `short_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flights`
--
ALTER TABLE `flights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `short_links`
--
ALTER TABLE `short_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
